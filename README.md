# Testeur Bash
__création__ : *octobre 2023* à Paris IX (Dauphine)   
__Licence__ :  Creative Commons BY SA   
__Auteur__ : Léon Piriou    

Contactez <leon.piriou@dauphine.eu> pour tout commentaires ou rectifications.

***
## Statut du projet
En finalisation, retste a faire fin doc + régler ligne 37 problème si bianire a extension avecc un '.' dans test_bin.bash

## Langage utilisé
* Bash


## Description rapide du projet
Création simplifiée de tests. Automatisation de l'éxécution d'un programme prenant en entrée les tests créés précédemment. Vérification des résultats avec la sortie attendue.


## Détails sur l'organisation des scripts
Tout le projet est contenu dans un dossier ```testeur_bash```, lequel contient :
* ```crea_test.bash```
* ```LISMOI.md```
* ```test_bin.bash```
* ```mult_tests.bash```
* ```a_tester```
* ```tests/```


Il est impératif de laisser ces scripts dans le même répertoire.

## Utilisation de _Testeur Bash_ depuis un terminal
1. [Création de tests](#creation-tests)
2. [Tests d'un programme simple](#tests-programme-simple)
3. [Tests de multiples programmes](#tests-programmes-multiples)

<br>
<br>

> ###### NOTES IMPORTANTES
>
> * On suppose pour tous les noms de dossiers que l'utilisateurs ne met pas de "/" à la fin.
>
> ex : **/home/user/mydir** (et pas /home/user/mydir/)
> 
> * Pour éxécuter ces scripts, il faut auparavant se placer dans le répertoire où ils sont enregistrés
>
> ex : **cd /home/.../testeur_bash** avant de lancer un des scripts
> <br>

<br>

<a id="creation-tests"></a>

### 1. Création de tests
Pour créer des tests, on éxécute :

>```./creation_tests.bash nbreTest nomTests Bool```

Avec :
* `nbreTest` correspondant aux nombre de tests différents que l'on souhaite créer
* `nomTests` correspondant au dossier où seront enregistré les tests créés. On a juste besoin du nom simple du test. Cet ensemble de tests sera enregistré dans ```tests/nomTests```.
* `Bool` vaut soit `1` soit `0`.
    * si `Bool` vaut `1`, le fichier où le test est enregistré est effacé avant d'écrire le nouveau test (uniquement si le test est déjà existant avec le même nom)
    * si `Bool` vaut `0`, le fichier où le test est enregistré est conservé et le nouveau test est ajouté à la suite (utile notamment si l'on souhaite compléter un test déjà existant)

<br>

Lors de l'éxécution on créé trois sous-dossier dans `tests/nomTests`:
* `./in` qui contient les fichiers avec les valeurs données en entrée au programme à tester.
* `./out` qui accueillera la sortie du programme pour les différents tests.
* `./theorique` qui contient les fichiers avec les valeurs attendues en sortie du programme à tester.

<br>

Il est ensuite demandé à l'utilisateur de saisir les valeurs pour les différents tests en alternant l'entrée avec la sortie associée attendue. Comme c'est spécifié, pour terminer la rédaction d'un test, il faut appuyer successivement sur les touches
> [Enter]

Puis
> Ctrl+D

<br>
<br>
<a id="tests-programme-simple"></a>

### 2. Tests d'un programme simple

Pour tester un programme, il faut préalablement créer des tests (voir [1. Création de tests](#creation-tests))

Ensuite, on éxécute :
> ```./test_bin.bash exe nomTests```

Avec:
* `exe` le chemin __depuis la racine__ de l'éxécutable à tester.
* `nomTests` le nom du dossier contenant les tests.

On éxécute alors `exe` avec les différents fichiers de tests créés précédemment, écrivant la sortie du programme pour chaque test dans un nouveau fichier localisé dans `nomTests/out`. De plus, on compare cette sortie avec celle attendue théoriquement.

L'affichage en console dit que le test est :
* `VALIDE` si la sortie du programme correspond avec celle attendue.
* `INVALIDE` si elle diffère de celle attendue.
* `ERREUR` si le code de retour du programme est différent de 0.

Enfin, un fichier `resume-exe.txt` localisé dans `nomTests` donne un résumé des tests réalisé, affichant les tests ayant donné une erreur(avec le code d'erreur) et ceux ayant produit des sortie invalides.

<br>
<br>
<a id="tests-programmes-multiples"></a>

### 3. Tests de multiples programmes

Créer un fichier contenant le nom en chemin absolu des éxécutables à tester suivis du dossier de tests de chaque éxécutable.

<u>Exemple</u> :

|file.txt                                           |
|:--------------------------------------------------|
 > cheminAbsoluVersExe1 nomTests1 <br>
 > cheminAbsoluVersExe2 nomTests2 <br>
 > cheminAbsoluVersExe3 nomTests3 <br>
 > ...

 Exécutez ensuite :

 > ```./mult_tests.bash file.txt```

 Si aucun fichier n'est spécifié en argument, cela fait les tests décrit dans `a_tester`.

 Cela réalise ```test_bin.bash``` pour tous les programmes et dossiers de tests associés
