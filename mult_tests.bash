#! /bin/bash

# Automatisation de l'appel au script "test_exe" pour plusieurs programmes a tester

# arg1 ($1) : fichiers contenant tous les tests a faire avec les binaires

cd $(echo $0 | grep -o '^.*\/')		# On se place dans le repertoire des scripts

if [ $# -ne 1 ]; then
	echo "Tests dans le fichier 'a_tester'"
	f_tests="a_tester"
else
	f_tests="$1"
fi

# Remplissage d'un tableau contenant tous les binaires et dossiers de tests
j=0
for i in $(cut -f1 $f_tests);
do
	tab[$j]="$i"
	j=$j+1
done

# Execution des differents binaires avec leur dossier de test
for (( i=0 ; i < $j; i=$i+2 ))
do
	exe=${tab[$i]}
	dossier=${tab[$i+1]}
	
	echo "**Debut tests $exe********************"
	bash ./test_bin.bash "$exe" "$dossier"
	echo "**Fin tests $exe**********************"
	echo
done

