# Tuto du *testeur bash*


### 1. Présentation du programme `tuto_bin` :

Le programme `tuto_bin` code la fonction identité de R^3. Lors de l'éxécution, il attend 3 nombre (par un `scanf("%d %d %d\n", ...)`) et renvoie ces trois nombres.

Nous allons créer un ensemble de tests pour ce programme :

### 2. Fonctionnement de *Testeur Bash* pour un programme:

1) Ouvrir un terminal et aller dans le dossier contenant tous les scripts bash
<br>
ex : `cd /home/.../testeur_bash`
<br><br>

2) Mise en place des tests
    > ./crea_test.bash 3 id_R3 0

    On créé 3 tests rangé dans l'ensemble de tests nommé `id_R3` (c'est un dossier).
    Ici, comme c'est la première fois qu'on créé le test, on peut mettre indifféremment `0` ou `1`.

    Pour le test 1 on met `1 2 3` puis appuyer sur `Enter` et après sur `Ctrl + D` pour finir la saisie du test 1. Comme sortie, on met `1 2 3` comme attendu pour l'identité de R^3.

    On refait de même avec en entrée  et en sortie `-2 -8 -36` et pour le dernier test, on met en entrée et en sortie `9000000000 50 6`.
<br><br>

3) On test ensuite notre programme :
    > ./test_bin.bash /home/.../testeur_bash/tuto_bin id_R3

    Notez que le programme tuto_bin est donné en chemin absolu depuis la racine. Cela ne fonctionnera pas autrement.
<br><br>

4) Analyse des résultats :

Dans notre cas, la dernière sortie est invalide car on avait stocké nos nombres en entrée dans des `int` (en C).

Il est affiché le numéro du test :
 > == X ======

Puis le statut de ce dernier :
 > VALIDE

 ou 
 > INVALIDE
 
 avec en-dessous l'affichage de la sortie de ce que renvoie le programme suivit de ce qui est attendu comme sortie (affichage de la commande `diff` entre la sortie de programme et la sortie attendue).

 ou
  > ERREUR

  si le programme se termine avec un code de retour différent de 0.

  On peut ensuite avoir un résumé de tous les tests effectué dans le fichier
  `.../testeur_bash/tests/id_R3/resume-tuto_bin.txt` avec uniquement les tests invalide et ceux ayant donné une erreur (avec le code de retour du programme dans ce cas). Si l'on souhaite examiner les sorties données par notre programme plus précisémment, on peut se rendre dans `.../testeur_bash/tests/id_R3/out`.


  ### 2. Fonctionnement de *Testeur Bash* pour plusieurs programmes :

  Il faut pour cela créer un ensemble de tests pour chaque programme à tester (on peut aussi utiliser le même ensemble de tests pour plusieurs programmes si cela nous conivent) via `./crea_test.bash`
  
  Ensuite, on peut remplir `.../testeur_bash/a_tester` ou un autre fichier que l'on mettra en argument de `./mult_tests.bash` (pas besoin de mettre `a_tester` en argument, il l'est pasr défaut).
  
  Le remplissage du fichier se fait comme expliqué dans le `LISMOI.md`
