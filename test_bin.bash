#!/bin/bash

# On suppose que l'utilisateur ne met pas de "/" à la fin du nom de ses fichiers

# Pour que ca fonctionne, il faut un fichier contenant 3 fichiers pour les tests :
#	- 1 nomme 'in' dans lequel on met les fichiers de tests a mettre en entree du programme
#	- 1 nomme 'out' dans lequel on ecrira la sortie du programme
#	- 1 nomme theorique avec les valeurs attendues
# Ce fichier contenant les 3 autres sera stocke dans la variable fichiers_tests
# L'utilisateur devra se contenter de remplir les dossiers "in" et "theorique"
#
# Le nom des fichiers de test doit etre "test_i.ext" avec "i" un indice concordant pour le test
# et la sortie theorique et "ext" doit etre :
#	- ".in" pour les fichiers dans le dossier in
#	- ".thq" pour les fichiers dans le dossier theorique
#	- le script ecrira des fichiers ".out" contenant la sortie du test i dans le dossier out
#
# Le script cree un resume des tests dans le dossier de tests donne en entree
#
# arg1 ($1) : emplacement (depuis la racine) du fichier binaire a tester
# arg2 ($2) : nom du dossier contenant les tests


# Nom depuis la racine attendu pour $exe

cd $(echo $0 | grep -o '^.*\/')		# On se place dans le repertoire des scripts

if [ $# -lt 2 ] ; then
	echo "Manque des arguments"
	echo "$0 exe dossierTests"
	exit
fi

cd ./tests
exe="$1"
fichiers_tests="$2"
exe_simple=$(echo "$exe" | grep -o '[^\/]\w*$')		# garde le nom simple de l'executable
resume="$2/resume-$exe_simple.txt"
# initialisation du resume des tests
> "$resume"

declare -i nbre_fichiers=`ls -l $fichiers_tests"/in" | wc -l`
let nbre_fichiers=nbre_fichiers-1   # retire la premiere ligne affichant le nombre de fichiers

val=0
inval=0
err=0
# Numerotation des fichiers à partir de 1
for (( i=1 ; i <= $nbre_fichiers ; i++ ))
do
	echo == $i ======
	f_entree="$fichiers_tests/in/test_$i.in"
	f_sortie="$fichiers_tests/out/test_$i.out"
	f_reference="$fichiers_tests/theorique/test_$i.thq"
	
	"$exe" < "$f_entree" > "$f_sortie"
	code_retour="$?"
	
	if [ "$code_retour" -ne 0 ] ; then
		echo "ERREUR"
		((err=err+1))
		echo "prgrm [$exe] | test $i : erreur code : $code_retour" >> "$resume"
	elif diff "$f_sortie" "$f_reference" ; then
		echo "VALIDE"
		((val=val+1))
	else
		echo "INVALIDE"
		((inval=inval+1))
		echo "prgrm [$exe] | test $i : invalide" >> "$resume"
	fi
done

echo
echo "$nbre_fichiers tests effectués. $val valide. $inval invalide. $err erreur."

