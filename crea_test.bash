#! /bin/bash


# On suppose que l'utilisateur ne met pas de "/" à la fin du nom de ses dossiers

# Permet de creer des fichiers de test a la chaine a utiliser pour tester un programme
#
# arg1 ($1) : nombre de tests à faire
# arg2 ($2) : dossiers des tests
# arg3 ($3) : si non nul, supprime le contenu des fichiers de test (si nul, le fichier test 
#		n'est pas supprime

cd $(echo $0 | grep -o '^.*\/')		# On se place dans le repertoire des scripts

if [ $# -lt 3 ] ; then
	echo "Manque les arguments"
	echo "$0 nbreTests nomDossierTests VideTest:bool"
	exit
fi


#clear
cd ./tests
mkdir $2 "$2/in" "$2/out" "$2/theorique"

echo 'Utiliser [Enter] puis [Ctrl + D] pour arreter la creation du fichier en cours'
for (( i=1 ; i<=$1 ; i++ ))
do
	echo
	f_entree="$2/in/test_$i.in"
	f_reference="$2/theorique/test_$i.thq"
	
	if [ "$3" -ne 0 ]; then
		# Suppression du contenu des tests précédents
		> "$f_entree" > "$f_reference"
	fi
	echo "[test $i]   $f_entree: "
	cat - >> "$f_entree"
	
	echo "[sortie attendue $i]   $f_reference: "
	cat - >> "$f_reference"
done

